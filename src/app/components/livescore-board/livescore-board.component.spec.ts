import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivescoreBoardComponent } from './livescore-board.component';

describe('LivescoreBoardComponent', () => {
  let component: LivescoreBoardComponent;
  let fixture: ComponentFixture<LivescoreBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivescoreBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivescoreBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
