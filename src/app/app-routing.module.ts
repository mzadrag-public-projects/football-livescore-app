import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LivescoreBoardComponent } from './components/livescore-board/livescore-board.component';

const routes: Routes = [
  { path: '', component: LivescoreBoardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
